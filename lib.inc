section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        jz .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax        ; string length
    mov rax, 1          ; system call number
    mov rsi, rdi        ; where does string start
    mov rdi, 1          ; where to write
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1          ; system call number
    mov rdi, 1          ; where to write
    mov rsi, rsp        ; where does string start
    mov rdx, 1          ; string length
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi            ; число в rax
    mov r10, 10             ; основание сс
    mov r9, rsp
    dec rsp
    push 0
    
    .while:
        xor rdx, rdx
        div r10                 ; делим на 10
        add rdx, 48             ; to ASCII
        dec rsp
        mov byte [rsp], dl      ; остаток деления в стек
        cmp rax, 0              ; если есть еще, что делить
        jne .while              ; то делим

    mov rdi, rsp
    push r9
    call print_string
    pop r9
    mov rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0                  ; сравниваем с нулем
    jns .pos                    ; если положительное число, то печатаем число
    push rdi                    ; если - нет, то запоминаем число в стек
    mov rdi, '-'                ; заносим в регистр знак минус
    call print_char             ; печатаем его
    pop rdi
    neg rdi                     ; отрицательное число становится положительным
    
    .pos:
        jmp print_uint          ; печатаем число
    
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r10, r10
    xor r11, r11
    
    .loop:
        mov r10b, [rdi + rax]       ; в регистр заносим символ по адресу rdi+rax первой строки
        mov r11b, [rsi + rax]       ; в регистр заносим символ по адресу rdi+rax второй строки
        cmp r10, r11                ; сравниваем их
        jne .no                     ; если не равны, то в rax заносим 0
        inc rax                     ; если равны, то rax++
        cmp r10, 0                  ; если ноль, то в rax заносим 1
        jnz .loop
        
    mov rax, 1
    ret
    
    .no:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r8
    push r9
    push r10
    ; rsi - buff size, rdi - buff adr
    mov r8, rdi
    mov r9, rsi
    xor r10, r10
    
    .check_whitespaces:                     ; проверка на пробельные символы в начале
        call read_char
        cmp rax, 0
        je .ok
        mov byte [r8 + r10], al
        cmp byte [r8 + r10], 0x20
        je .check_whitespaces
        cmp byte [r8 + r10], 0x9
        je .check_whitespaces
        cmp byte [r8 + r10], 0xA
        je .check_whitespaces
    
    inc r10
        
    .loop:
        call read_char
        cmp al, 0x20
        je .ok
        cmp al, 0x9
        je .ok
        cmp al, 0xA
        je .ok
        cmp al, 0x0
        je .ok
        mov byte [r8 + r10], al             ; записываем символ в буфер
        cmp r9, r10                         ; проверяем размер буфера
        jl .err
        inc r10
        jmp .loop

    .ok:
        mov rdx, r10                        ; записываем длину слова
        mov rax, r8                         ; записываем адресс буффера
        mov byte [r8 + r10], 0              ; дописываем нуль-символ к концу слова
        pop r10
        pop r9
        pop r8
        ret

    .err:
        xor rax, rax                        ; 0 при неудаче
        pop r10
        pop r9
        pop r8
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r10, 10                             ; сс
    xor r9, r9                              ; длина
    xor r8, r8                              ; символ

    .loop:
        mov r8b, byte [rdi + r9]            ; заносим в регистр первый символ
        cmp r8b, 0x30                       ; сравниваем с '0'
        jl .end                             ; если меньше, значит не число
        cmp r8b, 0x39                       ; сравниваем с '9'
        jg .end                             ; если больше, значит не число

        sub r8b, 0x30
        mul r10                             ; умножаем на основание сс
        add rax, r8                         ; прибавляем к числу
        inc r9                              ; длина +1
        jmp .loop

    .end:
        mov rdx, r9                         ; заносим в rdx длину числа
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], 0x2D                    ; сравниваем первый символ с '-'
    je .neg
    jmp parse_uint                          ; если положительное, то переходим к parse_uint

    .neg:
        inc rdi                             ; инкрементируем rdi для пропуска '-'
        call parse_uint
        neg rax                             ; полученное число инвертируем
        inc rdx                             ; rdx++, т.к. пропустили минус
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    
    .loop:
        cmp rax, rdx                    ; проверяем длину буффера
        jg .err
        mov r8, [rdi + rax]             ; в регистр заносим символ
        mov [rsi + rax], r8             ; в буфер заносим символ
        cmp r8, 0                       ; сравниваем символ с нуль-символом
        je .end
        inc rax                         ; двигаем указатель
        jmp .loop

    .err:
        xor rax, rax
        ret
    .end:
        ret
